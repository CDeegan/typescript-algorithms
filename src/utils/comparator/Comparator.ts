export default class Comparator {
	/**
	 * Default comparison function. It just assumes that "a" and "b" are strings or numbers.
	 */
	private compare: (a: number | string, b: number | string) => number = (a: number | string, b: number | string) => {
		if (a === b) return 0;
		return a < b ? -1 : 1;
	}

	/**
	 * Allow for custom compare function, e.g. compare custom objects together.
	 */
	constructor(compareFunction?: (a: any, b: any) => number) {
		if (compareFunction) this.compare = compareFunction;
	}

	/**
	 * Checks if two variables are equal.
	 */
	equal(a: any, b: any): boolean {
		return this.compare(a, b) === 0;
	}

	/**
	 * Checks if variable "a" is less than "b".
	 */
	lessThan(a: any, b: any): boolean {
		return this.compare(a, b) < 0;
	}

	/**
	 * Checks if variable "a" is greater than "b".
	 */
	greaterThan(a: any, b: any): boolean {
		return this.compare(a, b) > 0;
	}

	/**
	 * Checks if variable "a" is less than or equal to "b".
	 */
	lessThanOrEqual(a: any, b: any): boolean {
		return this.lessThan(a, b) || this.equal(a, b);
	}

	/**
	 * Checks if variable "a" is greater than or equal to "b".
	 */
	greaterThanOrEqual(a: any, b: any): boolean {
		return this.greaterThan(a, b) || this.equal(a, b);
	}

	/**
	 * Reverses the comparison order.
	 */
	reverse(): void {
		const compareOriginal = this.compare;
		this.compare = (a: any, b: any) => compareOriginal(b, a);
	}
}
