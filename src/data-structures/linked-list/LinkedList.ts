import LinkedListNode from './LinkedListNode';
import Comparator from '../../utils/comparator/Comparator';

export default class LinkedList {
	public head: LinkedListNode | null;
	public tail: LinkedListNode | null;
	private compare: Comparator;

	constructor(comparatorFunction?: (a: any, b: any) => number) {
		this.head = null;
		this.tail = null;
		this.compare = new Comparator(comparatorFunction);
	}

	prepend(value: any): LinkedList {
		// Set head as a new node.
		const newNode = new LinkedListNode(value, this.head);
		this.head = newNode;

		/// If tail does not exist, use new node as tail.
		if (!this.tail) {
			this.tail = newNode;
		}

		return this;
	}

	append(value: any): LinkedList {
		const newNode = new LinkedListNode(value);

		// If head does not exist, use new node as head.
		if (!this.head) {
			this.head = newNode;
			this.tail = newNode;
			return this;
		}

		// Attach new node to end of linked list.
		this.tail!.next = newNode;
		this.tail = newNode;

		return this;
	}

	delete(value: any): LinkedListNode | null {
		// When no head exists, there is nothing to delete.
		if (!this.head) return null;

		// When head is deleted, new head needs to be set to next node in linked list.
		let deletedNode = null;
		while (this.head && this.compare.equal(this.head.value, value)) {
			deletedNode = this.head;
			this.head = this.head.next;
		}

		let currentNode = this.head;
		if (currentNode !== null) {
			// Check next of each node, if it is to be deleted then point that next node to its own next node.
			while (currentNode!.next) {
				if (this.compare.equal(currentNode!.next.value, value)) {
					deletedNode = currentNode!.next;
					currentNode!.next = currentNode!.next.next;
				} else {
					currentNode = currentNode!.next;
				}
			}
		}

		// Check if tail must be deleted.
		if (this.compare.equal(this.tail!.value, value)) this.tail = currentNode;

		return deletedNode;
	}

	find(args: { value?: any, callback?: (value: any) => boolean }): LinkedListNode | null {
		if (!this.head) return null;

		let currentNode = this.head;

		while (currentNode) {
			// If callback is specified, try to find node with it.
			if (args.callback && args.callback(currentNode.value)) return currentNode;

			// If value is specified, try to compare by value.
			if (args.value !== undefined && this.compare.equal(currentNode.value, args.value)) return currentNode;

			currentNode = currentNode.next;
		}

		return null;
	}

	deleteTail(): LinkedListNode | null {
		const deletedTail = this.tail;

		// If only one node exists, it must be the tail.
		if (this.head === this.tail) {
			this.head = null;
			this.tail = null;
			return deletedTail;
		}

		// Go to second last node, its next node is to be deleted.
		let currentNode = this.head;
		while (currentNode!.next) {
			if (!currentNode!.next.next) {
				currentNode!.next = null;
			} else {
				currentNode = currentNode!.next;
			}
		}

		this.tail = currentNode;

		return deletedTail;
	}

	deleteHead(): LinkedListNode | null {
		if (!this.head) return null;

		const deletedHead = this.head;

		if (this.head.next) {
			this.head = this.head.next;
		} else {
			this.head = null;
			this.tail = null;
		}

		return deletedHead;
	}

	fromArray(values: any[]) {
		values.forEach(value => this.append(value));
		return this;
	}

	toArray(): LinkedListNode[] {
		const nodes = [];

		let currentNode = this.head;
		while (currentNode) {
			nodes.push(currentNode);
			currentNode = currentNode.next;
		}

		return nodes;
	}

	toString(callback?: (value: any) => string) {
		return this.toArray()
			.map((node) => node.toString(callback))
			.toString();
	}

	reverse(): LinkedList {
		let currNode = this.head;
		let prevNode = null;
		let nextNode = null;

		while (currNode) {
			// Store next node.
			nextNode = currNode.next;

			// Swap next node with previous node.
			currNode.next = prevNode;

			// Move previous and current nodes one step forward.
			prevNode = currNode;
			currNode = nextNode;
		}

		// Reset head and tail.
		this.tail = this.head;
		this.head = prevNode;

		return this;
	}
}
